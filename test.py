""" import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
 """
from car.databases import client

mongo_lsv = client.MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="persons"))
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="person"
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="vehicle"
    )
)
"""print(
    mongo_lsv.update_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_query={"_id": ObjectId("62a77cf8fb2b183d4bee7084")},
        record_new_value={"label": "Número de Identificación Tributario"},
    )
)"""
"""print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record_id="62a77db856ddffddd49eb13d",
    )
)"""
"""print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="documents_types",
        record={"label": "Numero de identificacion Tributario", "value": "NIT"},
    )
)"""
