from bson.objectid import ObjectId
import sys

sys.path.append("..")
from databases.client import MongoLsv
mongo_lsv = MongoLsv()

print(
    mongo_lsv.delete_record_in_collection(
        db_name="persons",
        collection="person",
        record_id="62adf8915d87f7e4547398c1",
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="person"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record_id="62adf8915d87f7e4547398c6",
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="vehicle"
    )
)
