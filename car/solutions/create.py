from bson.objectid import ObjectId
import sys

sys.path.append("..")
from databases.client import MongoLsv


mongo_lsv = MongoLsv()

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="person",
        record={
            "first_name": 'Isaac',
            "last_name": 'Benavidez',
            "age": 21,
            "email": 'isaacxzx@gmail.com',
            "phone": 123456789,
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="person",
        record={
            "first_name": 'Antonio',
            "last_name": 'Acuña',
            "age": 27,
            "email": 'acuna@gmail.com',
            "phone": 123456789,
        }
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="person",
        record={
            "first_name": 'Franklin',
            "last_name": 'Sarmiento',
            "age": 39,
            "email": 'franklins@gmail.com',
            "phone": 123456789,
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="person",
        record={
            "first_name": 'Angie',
            "last_name": 'Vazquez',
            "age": 14,
            "email": 'avazquez@gmail.com',
            "phone": 123456789,
        }
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="person",
        record={
            "first_name": 'Vanessa',
            "last_name": 'Lopez',
            "age": 30,
            "email": 'vanelopez@gmail.com',
            "phone": 123456789,
        }
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="person"
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record={
            "brand": "Toyota",
            "model": "Corolla",
            "year": 2019,
            "person": ObjectId('62ad50f3feb19fde2ca88ef1'),
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record={
            "brand": "Ford",
            "model": "Mustang",
            "year": 2015,
            "person": ObjectId('62ad50f3feb19fde2ca88ef2'),
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record={
            "brand": "Chevrolet",
            "model": "Spark",
            "year": 2018,
            "person": ObjectId('62ad50f3feb19fde2ca88ef1'),
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record={
            "brand": "Honda",
            "model": "Civic",
            "year": 2017,
            "person": ObjectId('62ad50f3feb19fde2ca88ef3'),
        }
    )
)
print(
    mongo_lsv.create_new_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record={
            "brand": "Nissan",
            "model": "Versa",
            "year": 2018,
            "person": ObjectId('62ad50f3feb19fde2ca88ef4'),
        }
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="vehicle"
    )
)
