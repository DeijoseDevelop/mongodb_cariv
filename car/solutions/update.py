from bson.objectid import ObjectId
import sys

sys.path.append("..")
from databases.client import MongoLsv

mongo_lsv = MongoLsv()
print(
    mongo_lsv.update_record_in_collection(
        db_name="persons",
        collection="person",
        record_query={"_id": ObjectId('62adf8915d87f7e4547398c1')},
        record_new_value={
            '_id': ObjectId('62adf8915d87f7e4547398c1'),
            'first_name': 'Andres',
            'last_name': 'Garcia',
            'age': 22,
            'email': 'andresG@gmail.com',
            'phone': 123456789
        },
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="person"
    )
)

print(
    mongo_lsv.update_record_in_collection(
        db_name="persons",
        collection="vehicle",
        record_query={"_id": ObjectId('62adf8915d87f7e4547398c6')},
        record_new_value={
            "brand": "Ford",
            "model": "Mustang",
            "year": 2015,
            "person": ObjectId('62ad50f3feb19fde2ca88ef1'),
        }
    )
)
print(
    mongo_lsv.get_records_from_collection(
        db_name="persons", collection="vehicle"
    )
)