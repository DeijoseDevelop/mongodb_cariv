from dataclasses import asdict, dataclass

@dataclass
class Person(object):
    uuid: str
    first_name: str
    last_name: str
    age: int
    email: str
    phone: int

    def to_dict(self) -> dict:
        return asdict(self)

@dataclass
class Vehicle(object):
    uuid: str
    brand: str
    model: str
    year: int
    person: Person

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_person(self):
        return self.person.first_name + ' ' + self.person.last_name
